inkscape-mobile-export
========

This Inkscape extension exports all selected items in different densities. The exported PNGs will be named by their prefix or ID in the SVG. The extension generate icons for iOS and Android.

Install
--------

Download `mobile_export.inx` and `mobile_export.py` and copy both files to your `$HOME/.config/inkscape/extensions` folder. A restart of Inkscape is required.

Usage
--------

* Select at least one item to export
* Select `Extensions -> Export -> Mobile Export…`
* Customize the settings according to your needs

License
========

    Copyright 2017 Emilio Frusciante (FEj) - emilio[dot]frusciante[at]gmail[dot]com

    Licensed under the GNU GENERAL PUBLIC LICENSE, Version 3.0 (the "GPL");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

       https://www.gnu.org/licenses/gpl-3.0.txt

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

