#!/usr/bin/env python
#
# Author: Emilio Frusciante (FEj) - emilio[dot]frusciante[at]gmail[dot]com
# Version 0.0.1
# Date: 20170706
#
# ***** BEGIN GPL 3.0 LICENSE BLOCK *****
#
# Copyright 2017 Emilio Frusciante (FEj) - emilio[dot]frusciante[at]gmail[dot]com
# 
# Licensed under the GNU GENERAL PUBLIC LICENSE, Version 3.0 (the "GPL");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    https://www.gnu.org/licenses/gpl-3.0.txt
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# ***** END GPL 3.0 LICENCE BLOCK *****

import optparse
import sys
import os
import subprocess
from copy import copy
try:
  from subprocess import DEVNULL
except ImportError:
  DEVNULL = open(os.devnull, 'w')

def checkForPath(command):
  return 0 == subprocess.call([
                                command, "--version"
                              ], stdout=DEVNULL, stderr=subprocess.STDOUT)

def error(msg):
  sys.stderr.write((unicode(msg) + "\n").encode("UTF-8"))
  sys.exit(1)

def getOs(widthName):
  value = widthName[0]
  return {
    'i': 'ios',
    'a': 'android'
  }[value]

def export(svg, options):
  for width in options.widths:
    export_width(svg, options, width)

def export_width(svg, options, width):
  (widthName, widthSize) = width
  dir = "%s/%s" % (options.resdir, getOs(widthName))

  if not os.path.exists(dir):
    os.makedirs(dir)

  def export_resource(param, name):
    png = "%s/%s_%s.png" % (dir, name, widthSize)

    subprocess.check_call([
                            "inkscape",
                            "--without-gui",
                            param,
                            "-w=%s" % widthSize,
                            "--export-png=%s" % png,
                            svg
                          ], stdout=DEVNULL, stderr=subprocess.STDOUT)

    if options.strip:
      subprocess.check_call([
                              "convert", "-antialias", "-strip", png, png
                            ], stdout=DEVNULL, stderr=subprocess.STDOUT)
    if options.optimize:
      subprocess.check_call([
                              "optipng", "-quiet", "-o7", png
                            ], stdout=DEVNULL, stderr=subprocess.STDOUT)

  if options.source == '"selected_ids"':
    for id in options.ids:
      prefix = id
      if options.resprefix is not None and options.resprefix.strip():
        prefix = options.resprefix
      export_resource("--export-id=%s" % id, prefix)
  else:
    export_resource("--export-area-page", options.resname)

def check_boolstr(option, opt, value):
  value = value.capitalize()
  if value == "True":
    return True
  if value == "False":
    return False
  raise optparse.OptionValueError("option %s: invalid boolean value: %s" % (opt, value))

class Option(optparse.Option):
  TYPES = optparse.Option.TYPES + ("boolstr",)
  TYPE_CHECKER = copy(optparse.Option.TYPE_CHECKER)
  TYPE_CHECKER["boolstr"] = check_boolstr

def append_width(option, opt_str, value, parser, *width):
  if not value:
    return
  if getattr(parser.values, option.dest) is None:
    setattr(parser.values, option.dest, [])
  getattr(parser.values, option.dest).append(width)

class WidthGroup(optparse.OptionGroup):
  def add_width_option(self, name, width):
    self.add_option("--%s" % name, action="callback", type="boolstr", dest="widths", metavar="BOOL",
      callback=append_width, callback_args=(name, width), help="Export %s variants" % name.upper())

parser = optparse.OptionParser(usage="usage: %prog [options] SVGfile", option_class=Option)
parser.add_option("--source",  action="store", type="choice", choices=('"selected_ids"', '"page"'),  help="Source of the drawable")
parser.add_option("--id",      action="append", dest="ids", metavar="ID", help="ID attribute of objects to export, can be specified multiple times")
parser.add_option("--resdir",  action="store",  help="Resources directory")
parser.add_option("--resname", action="store",  help="Resource name (when --source=page)")
parser.add_option("--resprefix", action="store",  help="Resource prefix (default icon_)")

group = WidthGroup(parser, "Select which widths to export")
group.add_width_option("i20", 20)
group.add_width_option("i29", 29)
group.add_width_option("i40", 40)
group.add_width_option("i48", 48)
group.add_width_option("i50", 50)
group.add_width_option("i55", 55)
group.add_width_option("i57", 57)
group.add_width_option("i58", 58)
group.add_width_option("i60", 60)
group.add_width_option("i72", 72)
group.add_width_option("i76", 76)
group.add_width_option("i80", 80)
group.add_width_option("i87", 87)
group.add_width_option("i88", 88)
group.add_width_option("i100", 100)
group.add_width_option("i114", 114)
group.add_width_option("i120", 120)
group.add_width_option("i144", 144)
group.add_width_option("i152", 152)
group.add_width_option("i167", 167)
group.add_width_option("i172", 172)
group.add_width_option("i180", 180)
group.add_width_option("i196", 196)
group.add_width_option("i1024", 1024)
group.add_width_option("a36", 36)
group.add_width_option("a48", 48)
group.add_width_option("a72", 72)
group.add_width_option("a96", 96)
group.add_width_option("a144", 144)
group.add_width_option("a196", 196)
parser.add_option_group(group)

parser.add_option("--strip",  action="store",  type="boolstr", help="Use ImageMagick to reduce the image size")
parser.add_option("--optimize",  action="store",  type="boolstr", help="Use OptiPNG to reduce the image size")

(options, args) = parser.parse_args()
if len(args) != 1:
  parser.error("Expected exactly one argument, got %d" % len(args))
svg = args[0]

if options.resdir is None:
  error("No Mobile Resource directory specified")
if not os.path.isdir(options.resdir):
  error("Wrong Mobile Resource directory specified:\n'%s' is no dir" % options.resdir)
if not os.access(options.resdir, os.W_OK):
  error("Wrong Mobile Resource directory specified:\nCould not write to '%s'" % options.resdir)
if options.source not in ('"selected_ids"', '"page"'):
  error("Select what to export (selected items or whole page)")
if options.source == '"selected_ids"' and options.ids is None:
  error("Select at least one item to export")
if options.source == '"page"' and not options.resname:
  error("Please enter a resource name")
if not options.widths:
  error("Select at least one DPI variant to export")
if not checkForPath("inkscape"):
  error("Make sure you have 'inkscape' on your PATH")
if options.strip and not checkForPath("convert"):
  error("Make sure you have 'convert' on your PATH if you want to reduce the image size using ImageMagick")
if options.optimize and not checkForPath("optipng"):
  error("Make sure you have 'optipng' on your PATH if you want to reduce the image size using OptiPNG")

export(svg, options)
